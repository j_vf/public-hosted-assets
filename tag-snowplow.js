<script type="text/javascript" async=1>
        setTimeout(function ()
        {
                ;(function(p,l,o,w,i,n,g){if(!p[i]){p.GlobalSnowplowNamespace=p.GlobalSnowplowNamespace||[];p.GlobalSnowplowNamespace.push(i);p[i]=function(){(p[i].q=p[i].q||[]).push(arguments)};p[i].q=p[i].q||[];n=l.createElement(o);g=l.getElementsByTagName(o)[0];n.async=1;n.src=w;g.parentNode.insertBefore(n,g)}}
                (window,document,"script","http://d2obcfs3e9vqkg.cloudfront.net/F7Wkvrz3o75ja9.js","snowplow"))
                ;

                window.snowplow("newTracker", "F7Wkvrz3o75ja9", "ec2-34-207-81-238.compute-1.amazonaws.com",
                {
                        appId: "revitalash",
                        cookieDomain: "revitalash.com",
                        // discoverRootDomain: true, // default true
                        cookieSecure: false, // default true
                        respectDoNotTrack: false, // this is default, but docs don't explicitly say so.
                        pageUnloadTimer: 600, // default 500ms.
                        eventMethod: "beacon", // try "post" if "beacon" does not work.
                        maxLocalStorageQueueSize: 200, // default is 1000.
                        connectionTimeout: 6500, // default is 5000.
                        skippedBrowserFeatures: ['cd','pdf','qt','wma','dir','fla', 'java', 'gears', 'ag'], // default is [].
                        contexts:
                        {
                                webPage: true,
                                performanceTiming: true,
                                gaCookies: true,
                                geolocation: false, // if true, will prompt user for location.
                                clientHints: true,
                                optimizelyExperiments: false // use this later when A/B testing; & see other optimizely contexts.
                        }
                })
                ;
                window.snowplow('trackPageView');

                //// add these and others once initial trackers are functional.
                // window.snowplow('enableActivityTracking', 10, 10);
                // window.snowplow('enableLinkClickTracking');
        }, 0)
        ;
</script>
